---
marp: true
theme: adwaita
---

<!--
_class: title
_footer: CC BY-SA 4.0
-->

# Úvod do programování

Vojtěch Perník

---

# Úvod

* **program** – sada instrukcí, které počítač rozumí
<br>
* **bit** – číslice ve dvojkové soustavě
* **bajt** – osmiciferné číslo ve dvojkové soustavě, 8 bitů
<br>
* **zdrojový kód** – instrukce zapsané v programovacím jazyce
* **strojový kód** – instrukce zapsané v binární podobě

---

<!--
_class: title
-->

# Vývoj programovacích jazyků

Podle úrovně abstrakce nad programem

---

# 1. strojový kód

Seznam instrukcí v **binární podobě**

```
2104
1105
3106
7001
0053
FFFE
0000
```

---

# 1. strojový kód

![height:150px](./images/diagrams_Binary.png)

* **Procesor mu** přímo **rozumí**
* Je **závislý na** instrukční sadě daného **procesoru**

---

# 2. assembler

```assembler
ORG 100
LDA A
ADD B
STA C
HLT
DEC 83
DEC –2
DEC 0
END
```

* Narozdíl od binárního kódu je **lidsky čitelný**
* Instrukce mají **slovní označení**

---

# 3. programovací jazyky

```c
int main(void) {
    int a, b, c;
    a = 83;
    b = -2;
    c = a + b;
    return 0;
}
```

* Jejich uživatelům poskytují **abstrakci nad programem**
* Zaměřují se na to, jak **program vidí člověk**, ne počítač

---

<!--
_class: title
-->

# Dělení programovacích jazyků

Podle způsobu překladu do strojového kódu

---

# 1. kompilované jazyky

![height:150px](./images/diagrams_Compiled.png)

* Zdrojový kód je překládán **kompilátorem** (překladatel)
* Kompilátor přečte **celý** zdrojový kód a vygeneruje podle něj strojový kód
* **Příklady:** C, C++, Rust, Vala

---

# 1. kompilované jazyky

## Výhody kompilace

* **rychlost** – Stačí jen počkat, než se kompilace dokončí. Rychlost programu je pak srovnatelná s rychlostí v ASM.
* **nepřístupnost zdrojového kódu** – Program se šíří v podobě strojového kódu, nikdo tedy nemůže číst jeho zdrojový kód.
* **snadné odhalení chyb** – Kompilátor na chyby většinou upozorní dříve, než k nim dojde. Dokáže také předvídat nebezpečné situace.

---

# 1. kompilované jazyky

## Nevýhody kompilace

* **závislost na platformě** – Strojový kód je závislý na konkrétním procesoru a operačním systému, pro které byl určen.
* **nemožnost editace** – Když program zkompilujete, už ho nelze upravovat jinak, než opětovnou kompilací.
* **správa paměti** – Kompilátor sice dokáže spoustu chyb odhalit, problémy spojené se správou paměti ale často odhalit nedokáže.

---

# 2. interpretované jazyky

![height:150px](./images/diagrams_Interpreted.png)

* Zdrojový kód je překládán **interpretem** (tlumočník)
* Narozdíl od kompilátoru **nepřekládá celý program najednou**
* Vždy přeloží **jen to, co je zrovna potřeba**, a to **za běhu** programu
* Na cílovém počítači **musí být nainstalovaný** interpret daného jazyka
* **Příklady:** Python, JavaScript, PHP

---

# 2. interpretované jazyky

## Výhody interpretace

* **přenositelnost** – Program je spustitelný na všech počítačích, kde je k dispozici interpret daného jazyka.
* **garbage collector** (více později) – Stará se za nás o správu paměti a usnadňuje nám tím vývoj.
* **stabilita** – Když interpret narazí na chybu, vykonávání programu ukončí.
* **jednoduchá editace** – Program není kompilován, můžeme tedy části programu upravovat za běhu.

---

# 2. interpretované jazyky

## Nevýhody interpretace

* **rychlost** – Interpretace je výkonově náročnější, program tedy běží pomaleji.
* **často obtížné hledání chyb** – Jelikož interpret nečte celý kód najednou, chyby se projeví až ve chvíli, kdy na ně interpret narazí. Jejich hledání je tedy složitější.
* **přístupnost zdrojového kódu** – Program se šíří v podobě zdrojového kódu, ten tedy může kdokoli číst.

---

# 3. jazyky s virtuálním strojem

![height:125px](./images/diagrams_VirtualMachine.png)

* **Kombinace kompilace a interpretace**
* Zdrojový kód je zkompilován do **mezikódu** (bajtkódu)
* Bajtkód je díky jednoduchosti rychle interpretován tzv. **virtuálním strojem**
* **Příklady:** Java, C#

---

# 3. jazyky s virtuálním strojem

## Výhody virtuálního stroje

* **rychlé odhalení chyb** – Díky kompilaci jednoduše odhalíme chyby.
* **stabilita** – Interpret nás včas upozorní před vykonáním nebezpečné operace.
* **slušná rychlost** – Rychlost virtuálního stroje se může blížit i rychlosti kompilovaných jazyků.
* **málo zranitelný kód** – Program se šíří v podobě bajtkódu, ten není jednoduše lidsky čitelný.

---

# Zdroje

- ITnetwork – Úvod do jazyka Java [cit. 2. 1. 2023]
  https://www.itnetwork.cz/java/zaklady/java-tutorial-uvod-do-jazyka-java

- Wikipedie – Just-in-time kompilace [cit. 2. 1. 2023]
  https://cs.wikipedia.org/wiki/Just-in-time_kompilace
