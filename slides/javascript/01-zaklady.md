---
marp: true
theme: adwaita
style: |
    section {
        --slide-clr-accent: var(--clr-bg-warning);
    }
---

<!--
_class: title
_footer: CC BY-SA 4.0
-->

# Základy JavaScriptu

Vojtěch Perník

![bg right:40% 50%](./images/js-logo.svg)

---

# Úvod

* Webové stránky byly **dříve pouze statické**
* V roce 1995 v Netscape vzniká jazyk **LiveScript** inspirovaný Javou
* Společnost Sun jazyk přejmenovala na **JavaScript** (obchodní tah)
* Standardizován byl v roce 1998 pod názvem **ECMAScript**
<br>
* Interpretem jazyka je **webový prohlížeč**
* Program tedy běží **na počítači uživatele** a může webovou stránku **měnit v reálném čase** a přidávat tak interaktivitu

---

# Node.js

* Umožňuje spouštět JavaScript i **mimo webový prohlížeč**
* Vytvořen pro použití na serverech ale vznikají v něm i jiné projekty
<br>
* **Jako svůj základ používá** JavaScriptový interpret prohlížeče Chrome **V8**

<br>

![height:150px](./images/nodejs-logo.svg)

---

# První program

```js
console.log("Hello, World!");
```

* `console` je **objekt**, který symbolizuje terminál

* `log()` je jeho **metoda**, která vypíše to, co jí předáme jako **parametr**

* Funkce a metody jsou **příkazy, které mají něco vykonat**

* `"Hello, World!"` je **hodnota**, kterou metodě jako parametr předáváme

* Je dobrým zvykem psát za všemi příkazy **středník**, i když to v JS není nutné

---

# Příklad: zadání

Vytvořte program, který do konzole vypíše text „*Umím programovat!*“.

```
Umím programovat!
```

---

# Příklad: řešení

```js
console.log("Umím programovat!");
```

---

# Proměnné

Proměnná je **místo v paměti**, kam si můžeme **uložit nějakou hodnotu**

* Před použitím je nutné proměnnou **nadeklarovat** (vytvořit v paměti)
* **Přiřazení hodnoty** se provadí **pomocí jednoho rovnítka**
* Obojí lze provést také na jednom řádku

```js
let text; // deklarace
text = "Hello, World!"; // přiřazení
```
```js
// deklarace a přiřazení na jednom řádku
let text = "Hello, World!";
```

---

# Příklad: zadání

Vytvořte program, který nadeklaruje proměnnou `pozdrav` s hodnotou `"Ahoj!"`.

---

# Příklad: řešení

```js
let pozdrav = "Ahoj!";
```

---

# Hodnota proměnné

Čtení hodnoty proměnné:

```js
let text = "Hello, World!";
console.log(text);
```

* Data, která počítač chápe jako sekvenci bajtů
* Každá hodnota má svůj **datový typ** (text, číslo, …)

---

# Primitivní datové typy

* Hodnota **přímo uložena** v paměti
<br>
* `string` – textový řetězec, značí se uvozovkami
* `number` – číslo (celé nebo desetinné)
* `boolean` – pravda / nepravda (`true` / `false`)

```js
let s = "Hello, World!";
let n = -1.3;
let b = true;
```

---

# Referenční datové typy

* V paměti není hodnota uložena přímo, ale pouze **odkaz na místo v paměti**
* Všechny referenční datové typy jsou v JS typu `Object`

---

# Dělení podle typového systému

## Statický typový systém

* Ve staticky typovaných jazycích musíme při deklaraci proměnné **určit, jaký bude její datový typ**, a přiřadit do ní můžeme **pouze hodnoty daného typu**

## Dynamický typový systém

* V dynamicky typovaných jazycích lze do proměnných ukládat **jakýkoli typ**
<br>
* JavaScript je **dynamicky typovaný** jazyk

---

# Příklad: zadání

Vytvořte program, který nadeklaruje proměnnou `pozdrav` s hodnotou `"Ahoj!"` a hodnotu této proměnné vypíše.

```
Ahoj!
```

---

# Příklad: řešení

```js
let pozdrav = "Ahoj!";
console.log(pozdrav);
```

---

# Knihovny

* Knihovna je **soubor předpřipravených částí kódu**, které může programátor využít ve vlastních programech
* Knihovny plní úkol, který je potřeba řešit na více místech
<br>
* V JS se pro instalaci knihoven používá **NPM** (Node Package Manager)

---

# Použití knihoven

* Před použitím je nutné **nainstalovat** daný **modul** pomocí příkazu `npm install`
* V programu je pak potřeba **naimportovat** funkce, které chceme používat
<br>
* **Moduly** umožňují program **rozdělit do více souborů**, moduly totiž mohou být importovány z jiných modulů
* Nejjednoduším způsobem, jak udělat z JS souboru modul, je změna koncovky na `.mjs`

---

# Čtení z konzole

* Pro načítání vstupu z konzole využijeme knihovnu `readline-sync`

* Pro její instalaci je třeba spustit příkaz:
  ```
  npm install readline-sync
  ```

* V programu pak naimportujeme funkci `question()`:
  ```js
  import { question } from "readline-sync";
  ```

* Příponu souboru je nutné změnit na `.mjs`

---

# Čtení z konzole

```js
import { question } from "readline-sync";
let jmeno = question("Zadejte své jméno: ");
console.log("Vaše jméno: " + jmeno);
```

<pre><code>Zadejte své jméno: <i>Jan Novák</i>
Vaše jméno: Jan Novák</code></pre>

* Funkce `question()` vypíše to, co jí předáme jako parametr, načte z konzole uživatelský vstup a tento vstup nám tzv. **vrátí** jako `string`
* **Návratová hodnota** funkce je jakási **odpověď**, výsledek procesu
* Pomocí znaménka `+` můžeme více `string`ů spojit

---

# Spojování řetězců

Máme několik možností jak spojovat řetězce v JavaScriptu:

* Řetězce můžeme jednoduše sčítat pomocí znaménka `+`
* Můžeme využít tzv. interpolace řetězce a vložit přímo do řetězce proměnné

```js
let str_a = "první část";
let str_b = "druhá část";

let a = str_a + " prostřední část " + str_b;
let b = `${str_a} prostřední část ${str_b}`;
```

---

# Příklad: zadání

Vytvořte program, který se uživatele zeptá na jméno a jeho vlastnost a tyto informace vypíše.

<pre><code>Jak se jmenuješ? <i>Bill Gates</i>
Jaký jsi? <i>hustodémonsky bohatý</i>
Bill Gates je hustodémonsky bohatý</code></pre>

---

# Příklad: řešení

```js
import { question } from "readline-sync";

let jmeno = question("Jak se jmenuješ? ");
let vlastnost = question("Jaký jsi? ");

console.log(`${jmeno} je ${vlastnost}`);
```

---

# Příklad: zadání

Vytvořte jednoduchou kalkulačku, program se zeptá uživatele na dvě čísla a následně vypíše jejich součet, rozdíl, součin a podíl.

<pre><code>Zadejte první číslo: <i>10</i>
Zadejte druhé číslo: <i>5</i>

Součet: 15
Rozdíl: 5
Součin: 50
Podíl:  2</code></pre>

---

# Příklad: chybné řešení

Pravděpodobně zvolené řešení:

```js
import { question } from "readline-sync";

let a = question("Zadejte první číslo: ");
let b = question("Zadejte druhé číslo: ");

console.log();

console.log(`Součet: ${a + b}`);
console.log(`Rozdíl: ${a - b}`);
console.log(`Součin: ${a * b}`);
console.log(`Podíl:  ${a / b}`);
```

---

# Příklad: chybné řešení

Výstup programu ale vzoru neodpovídá:

<pre><code>Zadejte první číslo: <i>10</i>
Zadejte druhé číslo: <i>5</i>

Součet: <b>105</b>
Rozdíl: 5
Součin: 50
Podíl:  2</code></pre>

---

# Příklad: chybné řešení

## Důvod chybného výstupu

* Vraťme se zpět k popisu funkce `question()`:
  > Funkce `question()` vypíše to, co jí předáme jako parametr, načte z konzole uživatelský vstup a tento vstup nám tzv. vrátí jako `string`
* Problémem je, že vstup z konzole funkce **vrací jako `string`**
* Sčítání textových řetězců pomocí znaménka `+` tyto řetězce spojí
* U ostatních operací JavaScript automaticky provede **typovou konverzi**

---

# Parsování

* Parsování je **převod z textové podoby** na nějaký specifický typ
<br>
* Např. interpret jazyka parsuje náš zdrojový kód, aby ho mohl spustit
<br>
* Parsování čísel v JavaSriptu se dá provést pomocí funkcí `parseInt()` (celá čísla) a `parseFloat()` (desetinná čísla)
  ```js
  console.log(typeof parseFloat("20.7"));
  ```
  ```
  number
  ```

---

# Příklad: řešení

```js
import { question } from "readline-sync";

let a = parseFloat(question("Zadejte první číslo: "));
let b = parseFloat(question("Zadejte druhé číslo: "));

console.log();

console.log(`Součet: ${a + b}`);
console.log(`Rozdíl: ${a - b}`);
console.log(`Součin: ${a * b}`);
console.log(`Podíl:  ${a / b}`);
```

---

# Parsování při načítání z konzole

* Modul `readline-sync` nám může usnadnit práci funkcemi `questionInt()` a `questionFloat()`, které parsování provedou za nás
  ```js
  import { questionFloat } from "readline-sync";

  let a = questionFloat("Zadejte první číslo: ");
  let b = questionFloat("Zadejte druhé číslo: ");

  console.log();

  console.log(`Součet: ${a + b}`);
  console.log(`Rozdíl: ${a - b}`);
  console.log(`Součin: ${a * b}`);
  console.log(`Podíl: ${a / b}`);
  ```

---

# Podmínky

* Podmínky nám umožňují v programu **reagovat na různé situace**
<br>
* Podmínky zapisujeme klíčovým slovem `if`, za kterým následují závorky s logickým výrazem
* Pokud je logický výraz pravdivý, provede se následující příkaz
* Pokud pravdivý není, jeho vykonávání se přeskočí

```js
let x = questionInt("Zadejte číslo: ");
if (x < 0)
    console.log("Zadané číslo je záporné!");
console.log("Program zde pokračuje dál…");
```

---

<style scoped>
    ._cols {
        display: flex;
        gap: 1rem;
        align-items: center;
    }

    ._col:last-child {
        flex-grow: 1;
    }
</style>

# Podmínky

<div class="_cols">

<div class="_col">

| operátor | popis            |
| -------- | ---------------- |
| ==       | rovnost          |
| !=       | nerovnost        |
| >        | větší než        |
| <        | menší než        |
| >=       | větší nebo rovno |
| <=       | menší nebo rovno |
| !        | negace           |

</div>

<div class="_col">

```js
if (x == y) ...
if (x != y) ...
if (x > y) ...
if (x < y) ...
if (x >= y) ...
if (x <= y) ...
if (!(x > y)) ...
```

</div>

</div>

---

# Podmínky

* Pokud chceme podmíněně provést více příkazů, použijeme složené závorky
  ```js
  let a = questionFloat("Zadejte první číslo: ");
  let b = questionFloat("Zadejte druhé číslo: ");

  if (b != 0) {
      console.log("Druhé číslo není nula, můžeme tedy dělit!");
      console.log(a / b);
  }
  ```

---

# Příklad: zadání

Vytvořte program, který nadeklaruje proměnnou `x` s hodnotou `0` nebo `1` a následně tuto hodnotu prohodí. Pokud je v proměnné `0`, nastaví její hodnotu na `1`. Pokud je v ní `1`, nastaví ji na `0`. Po prohození program hodnotu vypíše.

---

# Příklad: chybné řešení

Pravděpodobně zvolené nefunkční řešení:

```js
let x = 0;

if (x == 0) {
    x = 1;
}
if (x == 1) {
    x = 0;
}

console.log(x);
```

```
0
```

---

# Příklad: chybné řešení

## Důvod chybného výstupu

* V proměnné je na začátku `0`

* Následně hodnotu změníme na `1`

* Najednou ale platí druhá podmínka a hodnota se změní zpět na `0`

---

# Klíčové slovo `else`

```js
let x = 0;

if (x == 0) {
    x = 1;
} else {
    x = 0;
}

console.log(x);
```

* Pokud podmínka platí, provede se kód v prvním bloku, pokud podmínka neplatí, provede se kód v bloku `else`

---

# Zdroje

- ITnetwork – Úvod do JavaScriptu [cit. 13. 1. 2023]
  https://www.itnetwork.cz/javascript/zaklady/javascript-tutorial-uvod-do-javascriptu-nepochopeny-jazyk

- Wikipedie – Node.js [cit. 13. 1. 2023]
  https://en.wikipedia.org/wiki/Node.js
