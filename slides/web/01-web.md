---
marp: true
theme: adwaita
---

<!--
_class: title
_footer: CC BY-SA 4.0
-->

# Základy HTML a CSS

Vojtěch Perník

![bg right:40% 50%](./images/html-logo.svg)
![bg right:40% 50%](./images/css-logo.svg)

---

# Protokol HTTP

![](./images/http-diagram.svg)

* **Klient** (počítač uživatele) zašle **požadavek** na **server**
* Server tento požadavek **zpracuje** a jako **odpověď** pošle **stránku v HTML**
* Tato komunikace probíhá přes **protokol HTTP**
* **Protokol** je standard, který říká, **jak má probíhat přenost dat** mezi počítači

---

# Server

* Server je **počítač**, který poskytuje nějaké služby, nebo **program**, který tyto služby realizuje
<br>
* Program zpracovávající komunikaci přes HTTP označujeme jako **webový server** (webserver) <br>
  ![width:300px](./images/apache-logo.svg)
  ![width:250px](./images/nginx-logo.svg)

---

# Domény a DNS

* Každý počítač v síti má svou vlastní **IP adresu** (`2620:0:861:ed1a::1`)
* Tyto adresy jsou ale těžko zapamatovatelné, existují tedy **domény**
* Doména je jakýsi **alias pro IP adresu**
<br>
* Systém **DNS** slouží k **přiřazení** IP adresy doménám
  https://youtu.be/J44jvmoDxzo
<br>
* Na jednu IP adresu může směřovat i více domén, v takovém případě se o zobrazení správného webu stará webserver

---

# Adresa URL

* URL slouží ke specifikaci **umístění zdrojů informací na Internetu**
<br>
* Formát URL je celkem jednoduchý: **<span style="color: var(--clr-success);">https://</span><span style="color: var(--clr-destructive);">cs.wikipedia.org</span><span style="color: var(--clr-accent);">/wiki/URL</span>**
  * <strong style="color: var(--clr-success);">protokol</strong> – určuje způsob přenosu dat
  * <strong style="color: var(--clr-destructive);">doména / IP adresa</strong> – počítač, na který se má daný požadavek poslat
  * <strong style="color: var(--clr-accent);">cesta</strong> – umístění na serveru

---

# HTML

* HTML je **značkovací jazyk** pro tvorbu webových stránek
* Značky umožňují **dodat textu** určitý **význam** (nadpis, odstavec, tabulka, …)
<br>
* HTML **není programovací jazyk**, neumožňuje totiž provádět logické operace, souží poze k sestavování dokumentů

---

# První web

Vytvořte soubor `index.html` s následující strukturou:

```html
<!DOCTYPE html>

<html lang="cs">
    <head>
        <meta charset="UTF-8" />
        <title>Můj první web</title>
    </head>
    <body>Vítejte na mém prvním webu!</body>
</html>
```

---

# První web

Po zobrazení v prohlížeči by stránka měla vypadat nějak takto:

<div class="browser">
<div>
<span>localhost:5555</span>
</div>
<iframe scrolling="no" srcdoc='
<!DOCTYPE html>
<html lang="cs">
    <head>
        <meta charset="UTF-8" />
        <title>Můj první web</title>
    </head>
    <body>Vítejte na mém prvním webu!</body>
</html>
'></iframe>
</div>

---

# Tagy a elementy

* **Značkám** v HTML dokumentech se říká **tagy**
* Tagy se zapisují pomocí **špičatých závorek** (menší než `<`, větší než `>`)
<br>
* Tagy mohou být **párové** a **nepárové** a mohou mít své **atributy**
<br>
* Objektům, které prohlížeč **podle tagů vykreslí**, říkáme **elementy**

---

# Rozbor ukázky

* Prvním řádkem řekneme prohlížeči, že se jedná o HTML dokument
  ```html
  <!DOCTYPE html>
  ```
* Dále definujeme samotný HTML dokument pomocí **párového** tagu `<html>`
  ```html
  <html lang="cs">
  </html>
  ```
* Každý párový tag má vždy svůj **ukončovací tag s lomítkem** (`</html>`), který definuje jeho konec, mezi nimi je vložen jejich obsah
* Pomocí `lang="cs"` definujeme **atribut** (doplňující parametr tagu) `lang` s hodnotou `cs`

---

# Rozbor ukázky

* Dokument se vždy dělí na **dvě části**:
  * **hlavičku** `<head>` – obsahuje informace pro prohlížeč a vyhledávače
  * **tělo** `<body>` – samotný obsah stránky

* Tento řádek řekne prohlížeči, jakou znakovou sadu naše stránka používá, a umožní nám např. používat české znaky
  ```html
  <meta charset="UTF-8" />
  ```

* Jedná se o **nepárový tag**, který se dá ukončit pomocí **lomítka na konci**, to ale u nepárových tagů není povinné

---

# Rozbor ukázky

* Pomocí `<title>` nastavíme titulek stránky v prohlížeči
  ```html
  <title>Můj první web</title>
  ```

* Obsahem stránky je pak samotný text v `<body>`

---

<!--
_class: title
-->

# Základní HTML tagy

---

# Odstavec `<p>`

V HTML by všechen souvislý text měl být rozdělen do odstavců:

```html
<!DOCTYPE html>
<html lang="cs">
    <head>
        <meta charset="UTF-8" />
        <title>Můj první web</title>
    </head>
    <body>
        <p>Vítejte na mém prvním webu!</p>
        <p>Psát HTML se teprve učím,
           ale myslím, že mi to docela jde.</p>
    </body>
</html>
```

---

# Odřádkování `<br />`

<div class="browser">
<div>
<span>localhost:5555</span>
</div>
<iframe scrolling="no" srcdoc='
<!DOCTYPE html>
<html lang="cs">
    <head>
        <meta charset="UTF-8" />
        <title>Můj první web</title>
    </head>
    <body>
        <p>Vítejte na mém prvním webu!</p>
        <p>Psát HTML se teprve učím,
           ale myslím, že mi to docela jde.</p>
    </body>
</html>
'></iframe>
</div>

* Ukázka sice obsahovala víceřádkový text, zobrazil se ale na jednom řádku
* HTML tedy zalomení v dokumentu nerespektuje
* Pro odřádkování se používá nepárový tag `<br />`
  ```html
  <p>Psát HTML se teprve učím,<br />
     ale myslím, že mi to docela jde.</p>
  ```

---

<!--
_class: title
-->

# Zvýraznění

---

# Tagy `<em>` a `<strong>`

V HTML existuje mnoho tagů sloužících ke zvýrazňování textu:

* `<em>` – důležitý text, zobrazený kurzívou
* `<strong>` – ještě důležitější text, zobrazený tučně
<br>
* O tyto tagy se zajímají **vyhledávače** a berou je jako **klíčová slova**
<br>
* Dříve se pro tyto účely používaly tagy `<b>` a `<em>`, které ale nemají žádný vyšší význam, pouze mění styl

---

# Tagy `<em>` a `<strong>`

```html
<p>Pro zneškodnění výbušniny přestřihněte <strong>červený</strong> drát,
   modrý drát může zapříčinit explozi.</p>

<p>Spusťte editor registru příkazem <em>regedit.exe</em>. <strong>Neručím
   za případné škody!</strong></p>
```

<div class="browser">
<div>
<span>localhost:5555</span>
</div>
<iframe scrolling="no" srcdoc='
<!DOCTYPE html>
<html lang="cs">
    <head>
        <meta charset="UTF-8" />
        <title>Můj první web</title>
    </head>
    <body>
        <p>Pro zneškodnění výbušniny přestřihněte <strong>červený</strong> drát, modrý drát může zapříčinit explozi.</p>
        <p>Spusťte editor registru příkazem <em>regedit.exe</em>. <strong>Neručím za případné škody!</strong></p>
    </body>
</html>
'></iframe>
</div>

---

# Uzavírání tagů

* Tagy by měli být vždy uzavírány v obráceném pořadí, než byly otevřeny:
  ```html
  <p><strong><em>Kurzíva i tučný text.</em></strong></p>
  ```
* **Křížení tagů je chybou** a může způsobit **špatné vykreslení**:
  ```html
  <p><strong><em>Tento text se může zobrazit špatně.</strong></em></p>
  ```

---

# Podtržení `<u>`

Tag `<u>` slouží k podtržení textu:

```html
<p>Tento text bude <u>podtržený</u>!</p>
```

<div class="browser">
<div>
<span>localhost:5555</span>
</div>
<iframe scrolling="no" srcdoc='
<!DOCTYPE html>
<html lang="cs">
    <head>
        <meta charset="UTF-8" />
        <title>Můj první web</title>
    </head>
    <body>
        <p>Tento text bude <u>podtržený</u>!</p>
    </body>
</html>
'></iframe>
</div>

* Dnes už se moc nepoužívá

---

# Přeškrtnutí `<s>`

Přeškrtnutého textu docílíme použítím tagu `<s>`:

```html
<p>AKCE! Pizzu u nás tento víkend koupíte za <s>200 Kč</s> 100 Kč!</p>
```

<div class="browser">
<div>
<span>localhost:5555</span>
</div>
<iframe scrolling="no" srcdoc='
<!DOCTYPE html>
<html lang="cs">
    <head>
        <meta charset="UTF-8" />
        <title>Můj první web</title>
    </head>
    <body>
        <p>AKCE! Pizzu u nás tento víkend koupíte za <s>200 Kč</s> 100 Kč!</p>
    </body>
</html>
'></iframe>
</div>

---

# Zvýrazňovač `<mark>`

Tag `<mark>` vizuálně zvýrazní text pomocí podbarvení:

```html
<p>Za poslední rok se počet aktivních uživatelů prostředí GNOME
   <mark>zvýšil o 20 %</mark>!</p>
```

<div class="browser">
<div>
<span>localhost:5555</span>
</div>
<iframe scrolling="no" srcdoc='
<!DOCTYPE html>
<html lang="cs">
    <head>
        <meta charset="UTF-8" />
        <title>Můj první web</title>
    </head>
    <body>
        <p>Za poslední rok se počet aktivních uživatelů prostředí GNOME <mark>zvýšil o 20 %</mark>!</p>
    </body>
</html>
'></iframe>
</div>

* `<mark>` má pouze **vizuální efekt**, pro vyhledávače **nemá význam**

---

# Nadpisy `<h1>` až `<h6>`

* Nadpisy pomáhají strukturovat text
* Zapisují se párovými tagy `<h1>` až `<h6>`, máme tedy 6 úrovní nadpisů
<br>
* Nadpis `<h1>` by měl být na stránce vždy pouze jeden

---

<!--
_class: title
-->

# Velký projekt

---

<div class="bg-website"><iframe src="https://pervoj.gitlab.io/edu-personal-portfolio/index.html"></iframe></div>

---

<div class="bg-website"><iframe src="https://pervoj.gitlab.io/edu-personal-portfolio/dovednosti.html"></iframe></div>

---

<div class="bg-website"><iframe src="https://pervoj.gitlab.io/edu-personal-portfolio/kontakt.html"></iframe></div>

---

# Příklad: zadání

Vytvořte osobní prezentaci v dokumentu `index.html` využívající nadpisy 3 úrovní.

---

<div class="browser" style="height: 635px">
<div>
<span>localhost:5555</span>
</div>
<iframe scrolling="no" srcdoc='
<!DOCTYPE html>
<html lang="cs">
    <head>
        <meta charset="UTF-8" />
        <title>Richard Novák</title>
    </head>
    <body>
        <h1>Richard Novák</h1>
        <h2>O mně</h2>
        <p>Programování se věnuji od svých 12 let a celkem mě to baví.</p>
        <h2>Dovednosti</h2>
        <h3>HTML</h3>
        <p>HTML se teprve učím, ale už umím vytvořit stránky jako tyto.</p>
        <h3>CSS</h3>
        <p>CSS také celkem zvládám, design ale pro mě moc není.</p>
        <h3>JavaScript</h3>
        <p>JavaScript mi jde ze všech mých dovedností nejlépe.</p>
        <h2>Kontakt</h2>
        <p>Kontaktovat mě můžete na adrese <em>richard@richardnovak.cz</em>.</p>
    </body>
</html>
'></iframe>
</div>

---

# Příklad: řešení

```html
<h1>Richard Novák</h1>

<h2>O mně</h2>
<p>Programování se věnuji od svých 12 let a celkem mě to baví.</p>

<h2>Dovednosti</h2>
<h3>HTML</h3>
<p>HTML se teprve učím, ale už umím vytvořit stránky jako tyto.</p>
<h3>CSS</h3>
<p>CSS také celkem zvládám, design ale pro mě moc není.</p>
<h3>JavaScript</h3>
<p>JavaScript mi jde ze všech mých dovedností nejlépe.</p>

<h2>Kontakt</h2>
<p>Kontaktovat mě můžete na adrese <em>richard@richardnovak.cz</em>.</p>
```

---

# Obrázky `<img />`

```html
<img src="https://picsum.photos/150/150" alt="náhodný obrázek" />
```

<div class="browser">
<div>
<span>localhost:5555</span>
</div>
<iframe scrolling="no" srcdoc='
<!DOCTYPE html>
<html lang="cs">
    <head>
        <meta charset="UTF-8" />
        <title>Můj první web</title>
    </head>
    <body>
        <img src="https://picsum.photos/id/827/150/150" alt="náhodný obrázek" />
    </body>
</html>
'></iframe>
</div>

* Obrázky do HTML dokumentu vkládáme pomocí tagu `<img />`
* Tento nepárový tag má dva základní atributy:
  * `src` – cesta k souboru s obrázkem
  * `alt` – alternativní text obrázku, jeho popis pro čtečky a vyhledávače

---

# Příklad: zadání

Přidejte do dokumentu pod nadpis první úrovně fotku. Použijte vlastní, nebo stáhněte z odkazu níže. K fotce dopište stručný a výstižný alternativní text. Obrázky uložte do podadresáře `obrazky/`.

https://1url.cz/6r0rs

---

# Příklad: řešení

```html
<h1>Richard Novák</h1>

<img src="obrazky/fotka.jpg" alt="Richard Novák">

<h2>O mně</h2>
...
```

---

# Odkazy `<a>`

```html
<a href="https://cs.wikipedia.org/">Odkaz na Wikipedii</a>
```

* Odkazy slouží k navigaci uživatele mezi různými HTML dokumenty
* Umožňují rozsáhlý web rozdělit do více dokumentů:
  ```html
  <a href="druha-stranka.html">druhá stránka</a>
  ```

* `href` – hypertextový odkaz *(Hypertext REFerence)*

---

# Příklad: zadání

Rozdělte obsah dokumentu do více souborů tak, aby v jednotlivých souborech `index.html`, `dovednosti.html` a `kontakt.html` byla patřičná část.

- Nezapomeňte na základní strukturu HTML dokumentu
- Nadpis „Richard Novák“ odeberte a všechny ostatní nadpisy povyšte o jednu úroveň
- Obrázek přesuňte na konec souboru `index.html`
- Na konec `index.html` přidejte dva odkazy na další dvě stránky
- Na konec zbylých dvou stránek přidejte odkaz „Zpět“ vedoucí na `index.html`

---

```html
<!DOCTYPE html>
<html lang="cs">
    <head>
        <meta charset="UTF-8" />
        <title>Richard Novák</title>
    </head>
    <body>
        <h1>O mně</h1>
        <p>Programování se věnuji od svých 12 let a celkem mě to baví.</p>
        <img src="obrazky/fotka.jpg" alt="Richard Novák">
        <p>
            <a href="dovednosti.html">dovednosti</a>
            <a href="kontakt.html">kontakt</a>
        </p>
    </body>
</html>
```

---

```html
<!DOCTYPE html>
<html lang="cs">
    <head>
        <meta charset="UTF-8" />
        <title>Dovednosti | Richard Novák</title>
    </head>
    <body>
        <h1>Dovednosti</h1>
        <h2>HTML</h2>
        <p>HTML se teprve učím, ale už umím vytvořit stránky jako tyto.</p>
        <h2>CSS</h2>
        <p>CSS také celkem zvládám, design ale pro mě moc není.</p>
        <h2>JavaScript</h2>
        <p>JavaScript mi jde ze všech mých dovedností nejlépe.</p>
        <p><a href="index.html">Zpět</a></p>
    </body>
</html>
```

---

```html
<!DOCTYPE html>
<html lang="cs">
    <head>
        <meta charset="UTF-8" />
        <title>Kontakt | Richard Novák</title>
    </head>
    <body>
        <h1>Kontakt</h1>
        <p>Kontaktovat mě můžete na adrese
           <em>richard@richardnovak.cz</em>.</p>
        <p><a href="index.html">Zpět</a></p>
    </body>
</html>
```

---

# Tabulka `<table>`

- Slouží k **přehlednějšímu prezentování dat** uživateli

```html
<table border="1">
    <tr>
        <td>Buňka 1</td>
        <td>Buňka 2</td>
        <td>Buňka 3</td>
    </tr>
    <tr>
        <td>Buňka 4</td>
        <td>Buňka 5</td>
        <td>Buňka 6</td>
    </tr>
</table>
```

---

<div class="browser">
<div>
<span>localhost:5555</span>
</div>
<iframe scrolling="no" srcdoc='
<!DOCTYPE html>
<html lang="cs">
    <head>
        <meta charset="UTF-8" />
        <title>Můj první web</title>
    </head>
    <body>
        <table border="1">
            <tr>
                <td>Buňka 1</td>
                <td>Buňka 2</td>
                <td>Buňka 3</td>
            </tr>
            <tr>
                <td>Buňka 4</td>
                <td>Buňka 5</td>
                <td>Buňka 6</td>
            </tr>
        </table>
    </body>
</html>
'></iframe>
</div>

* Tag `<table>` **celou tabulku** vymezí
* Tagy `<tr>` představují **řádky** tabulky *(table row)*
* Do `<td>` píšeme obsah jednotlivých **buněk** *(table data)*
<br>
* Atribut `border` je **zastaralý způsob**, jak zobrazit rámeček bez znalosti CSS
<br>
* Existují také **buňky záhlaví**, stačí místo `<td>` **použít `<th>`**

---

# Příklad: zadání

Upravte `dovednosti.html` tak, aby se výpis dovedností zobrazoval v tabulce. Dodejte i obrázky.

- HTML: https://1url.cz/hrvoL
- CSS: https://1url.cz/7rvot
- JavaScript: https://1url.cz/CrvoM

---

# Příklad: zadání

<div class="browser" style="height: 450px">
<div>
<span>localhost:5555</span>
</div>
<iframe scrolling="no" srcdoc='
<!DOCTYPE html>
<html lang="cs">
    <head>
        <meta charset="UTF-8" />
        <title>Dovednosti | Richard Novák</title>
    </head>
    <body>
        <h1>Dovednosti</h1>
        <table>
            <tr>
                <td><img src="https://1url.cz/hrvoL" alt="" /></td>
                <td><img src="https://1url.cz/7rvot" alt="" /></td>
                <td><img src="https://1url.cz/CrvoM" alt="" /></td>
            </tr>
            <tr>
                <td>
                    <h2>HTML</h2>
                    <p>HTML se teprve učím, ale už umím vytvořit stránky jako tyto.</p>
                </td>
                <td>
                    <h2>CSS</h2>
                    <p>CSS také celkem zvládám, design ale pro mě moc není.</p>
                </td>
                <td>
                    <h2>JavaScript</h2>
                    <p>JavaScript mi jde ze všech mých dovedností nejlépe.</p>
                </td>
            </tr>
        </table>
        <p><a href="index.html">Zpět</a></p>
    </body>
</html>
'></iframe>
</div>

---

<style scoped>
    pre {
        position: absolute;
        top: -0.6em;
        left: 50%;
        transform: translateX(-50%);
    }
</style>

```html
<table>
    <tr>
        <td><img src="obrazky/html.svg" alt="" /></td>
        <td><img src="obrazky/css.svg" alt="" /></td>
        <td><img src="obrazky/js.svg" alt="" /></td>
    </tr>
    <tr>
        <td>
            <h2>HTML</h2>
            <p>HTML se teprve učím, ale už umím vytvořit stránky jako tyto.</p>
        </td>
        <td>
            <h2>CSS</h2>
            <p>CSS také celkem zvládám, design ale pro mě moc není.</p>
        </td>
        <td>
            <h2>JavaScript</h2>
            <p>JavaScript mi jde ze všech mých dovedností nejlépe.</p>
        </td>
    </tr>
</table>
```

---

<!--
_class: title
-->

# CSS

Stylování stránek

---

# CSS

* Jeho funkcí je umožnit **úpravu vzhledu** webových stránek
* Jedná se o jazyk speciálně vyvinutý pro stylování HTML dokumentů
<br>
* Zkratka pro **Cascading Style Sheet** (list kaskádových stylů)
  * kaskádový → funguje zde **dědičnost**
  * styly nastavené pro rodičovský element se propíšou do elementů uvnitř

---

# Propojení CSS s HTML dokumentem

- Vytvořte prázdný soubor `styl.css`

- Do hlavičky všech HTML dokumentů přidejte následující řádek:

```html
<head>
    ...
    <link rel="stylesheet" href="styl.css" />
    ...
</head>
```

* `<link />` je tag sloužící k určení vztahu mezi dokumentem a jinými soubory
  * `rel` – definuje typ vztahu mezi soubory *(relationship)*
  * `href` – hypertextový odkaz *(Hypertext REFerence)*

---

# Struktura CSS

- Klíčovými prvky CSS jsou **selektory**, **vlastnosti** a jejich **hodnoty**:

```css
selektor1 {
    vlastnost1: hodnota;
    vlastnost2: hodnota;
}

selektor2 {
    vlastnost1: hodnota;
    vlastnost2: hodnota;
}
```

---

# Selektory

* Umožňují **vybrat**, pro které **elementy** na stráce bude styl platit

* Nejjednodušší je selektor **typový**, ten vybere všechny elementy daného typu

* Např. tento selektor vybere všechny nadpisy první úrovně:
  ```css
  h1 {

  }
  ```

---

# Vlastnost `text-align`

```css
h1 {
    text-align: center;
}
```

- Tato vlastnost definuje **zarovnání textu** (všech *řádkových* elementů)

* `center` – zarovnání na střed
* `left` – zarovnání doleva
* `right` – zarovnání doprava
* `justify` – zarovnání do bloku
* `start` a `end` – zarovnání k logickému začátku / konci, podle toku písma

---

# Násobné selektory

Pro vybrání více typů prvků najednou můžeme uvést selektorů více:

```css
h1,
h2 {
    text-align: center;
}
```

* Dobrou praktikou ve všech IT oborech je **nepsat to samé vícekrát**

---

# Vlastnost `color`

```css
h1 {
    color: blue;
}
```

- Slouží k definování **barvy textu**

---

# Základní způsoby zápisu barev

## 1. textové konstanty

Pro základní barvy stačí v CSS zadat jejich název.

16 možností: `aqua`, `black`, `blue`, `fuchsia`, `gray`, `green`, `lime`, `maroon`, `navy`, `olive`, `purple`, `red`, `silver`, `teal`, `white`, `yellow`

* Většinou ošklivé barvy, tento zápis se tedy tolik nepoužívá

---

# Základní způsoby zápisu barev

## 2. funkce RGB

* Ve světelném spektru se každá barva dá **sestavit ze složek červené, zelené a modré**

* Výsledná barva se mění podle toho, **kolik v ní každé složky je**

![bg right:25% 100%](./images/rgb.svg)

---

# Základní způsoby zápisu barev

## 2. funkce RGB

```css
h1 {
    color: rgb(0, 0, 255);
}
```

* Funkce bere 3 argumenty, **množství červené, zelené a modré složky**

* Hodnotou je vždy celé číslo **od `0` do `255`**

![bg right:25% 100%](./images/rgb.svg)

---

# Základní způsoby zápisu barev

## 3. zápis RGB v šestnáctkové soustavě

```css
h1 {
    color: #0000FF;
}
```

* Šestnáctková soustava se skládá z 16 číslic, **`0`-`9`** a **`A`-`F`**
<br>
* V tomto zápisu platí pro jednu barevnou složku **2 cifry**
* Hodnotou každé barvy je tedy **`00`-`FF`**, tedy 256 možností
<br>
* Písmena mohou být i malá

---

# Příklad: zadání

Zarovnejte všechny nadpisy 1. a 2. úrovně na střed a nastavte jejich barvu textu na `#3584e4`.

---

# Příklad: řešení

```css
h1,
h2 {
    text-align: center;
    color: #3584e4;
}
```

---

# Vlastnost `background-color`

```css
h1 {
    background-color: blue;
}
```

- Slouží k nastavení **barvy pozadí** elementu

---

# Příklad: zadání

Nastavte barvu pozadí tělíčka stránky na `#fafafa`.

---

# Příklad: řešení

```css
body {
    background-color: #fafafa;
}
```

---

# Box bodel

* Prohlížeč bere každý element na stránce jako **obdélníkovou přihrádku**
* Každá přihrádka se skládá ze **4 částí**:
  * **content** – oblast s obsahem elementu
  * **padding** – mezera mezi rámečkem elementu a jeho obsahem
  * **border** – rámeček elementu, hranice
  * **margin** – mezera mezi rámečkem elementu a jeho okolím

![bg right:40% 100%](./images/boxmodel.png)

---

<style scoped>
    ._cols {
        display: grid;
        gap: 1rem;
        grid-template-columns: repeat(2, 1fr);
    }
</style>

# Vlastnosti box modelu

## Padding

- Padding rozšiřuje **vnitřní prostor** elementu
- Definuje **mezeru mezi obsahem** elementu a jeho **okrajem**

<div class="_cols">

```css
h1 {
    padding: 30px;
    background-color: green;
}
```

<div class="browser">
<iframe scrolling="no" srcdoc='
<!DOCTYPE html>
<html lang="cs">
    <head>
        <meta charset="UTF-8" />
        <title>Můj první web</title>
        <style>
            h1 {
                padding: 30px;
                background-color: green;
            }
        </style>
    </head>
    <body>
        <h1>Nadpis stránky s paddingem!</h1>
    </body>
</html>
'></iframe>
</div>

</div>

---

# Vlastnosti box modelu

## Padding

Vlastnosti definující velikost paddingu:

* `padding-top`, `padding-bottom`, `padding-left`, `padding-right`
  - nastavení pro horní, spodní, levý, pravý padding
* `padding-inline`, `padding-inline-start`, `padding-inline-end`, `padding-block`, `padding-block-start`, `padding-block-end`
  - nastavení pro logicky vodorovné, svislé strany, jejich začátek a konec
* `padding` – nastavení pro všechny strany pomocí jedné vlastnosti

---

<style scoped>
    ._cols {
        display: grid;
        gap: 1rem;
        grid-template-columns: repeat(2, 1fr);
    }
</style>

# Vlastnosti box modelu

## Margin

- Margin nastavuje **prostor mezi elementem a okolím**

<div class="_cols">

```css
h2 {
    margin: 30px;
    background-color: green;
}
```

<div class="browser">
<iframe scrolling="no" srcdoc='
<!DOCTYPE html>
<html lang="cs">
    <head>
        <meta charset="UTF-8" />
        <title>Můj první web</title>
        <style>
            h2 {
                margin: 30px;
                background-color: green;
            }
        </style>
    </head>
    <body>
        <h2>Nadpis stránky s marginem!</h2>
        <h2>Nadpis stránky s marginem!</h2>
    </body>
</html>
'></iframe>
</div>

</div>

---

# Vlastnosti box modelu

## Margin

Vlastnosti definující velikost marginu:

* `margin-top`, `margin-bottom`, `margin-left`, `margin-right`
  - nastavení pro horní, spodní, levý, pravý margin
* `margin-inline`, `margin-inline-start`, `margin-inline-end`, `margin-block`, `margin-block-start`, `margin-block-end`
  - nastavení pro logicky vodorovné, svislé strany, jejich začátek a konec
* `margin` – nastavení pro všechny strany pomocí jedné vlastnosti

---

# Vlastnosti box modelu

## Border

```css
h1 {
    /* border: <šířka> <typ> <barva>; */
    border: 2px solid red;
}
```

<div class="browser">
<iframe scrolling="no" srcdoc='
<!DOCTYPE html>
<html lang="cs">
    <head>
        <meta charset="UTF-8" />
        <title>Můj první web</title>
        <style>
            h1 {
                border: 2px solid red;
            }
        </style>
    </head>
    <body>
        <h1>Nadpis stránky s rámečkem!</h1>
    </body>
</html>
'></iframe>
</div>

---

# Vlastnosti box modelu

## Velikost elementu

* Velikost lze nastavovat buď pro **content box**, nebo **border box**
* Toto chování lze měnit pomocí vlastnosti `box-sizing`:
  ```css
  h1 {
      box-sizing: border-box; /* možnosti: content-box, border-box */
  }
  ```

---

# Vlastnosti box modelu

## Velikost elementu

Samotnou velikost můžeme definovat pomocí vlastností `width` a `height`:

* `width` – šířka elementu
* `height` – výška elementu
<br>
* `max-width`, `max-height` – maximální šířka, výška
* `min-width`, `min-height` – minimální šířka, výška

---

# Elementy blokové a řádkové

## Blokové elementy

* Blokové elementy se **roztáhnou na celou šířku** rodičovského elementu a **výšku přizpůsobí** obsahu

* Vždy jsou na **samostatném řádku**

* Blokové elementy můžou obsahovat řádkové i blokové elementy

* Příklady: *odstavce, nadpisy*

---

# Elementy blokové a řádkové

## Řádkové elementy

* Nezabírají více místa, než jejich obsah, a **řadí se do řádku**

* Mnohdy jsou **schopné se** na konci řádku **rozdělit**

* **Margin a padding** pro ně **platí jen ve směru toku textu**, v kolmém směru **přetékají do okolního textu**

* Příklady: *odkazy, tagy `<strong>` a `<em>`, obrázky*

---

# Elementy blokové a řádkové

Chování elementu můžeme změnit pomocí vlastnosti `display`:

```css
h1 {
    display: inline; /* základní možnosti: block, inline */
}
```

---

# Příklad: zadání

Nastavte, aby obrázky byly blokovými elementy a jejich maximální šířka `100%`.

---

# Příklad: řešení

```css
img {
    display: block;
    max-width: 100%;
}
```

---

# Zdroje

- Wikipedie – Server [cit. 23. 1. 2023]
  https://cs.wikipedia.org/wiki/Server

- Wikipedie – Hypertext Markup Language [cit. 23. 1. 2023]
  https://cs.wikipedia.org/wiki/Hypertext_Markup_Language

- ITnetwork – Úvod do HTML a váš první web [cit. 23. 1. 2023]
  https://www.itnetwork.cz/html-css/webove-stranky/jak-psat-moderni-web-html-tutorial-uvod-do-html

---

# Zdroje

- ITnetwork – Základní HTML tagy [cit. 23. 1. 2023]
  https://www.itnetwork.cz/html-css/webove-stranky/jak-psat-moderni-web-html-tutorial-zakladni-tagy

- ITnetwork – Obrázky a odkazy v HTML [cit. 6. 2. 2023]
  https://www.itnetwork.cz/html-css/webove-stranky/jak-psat-moderni-web-html-tutorial-obrazky-odkazy

- ITnetwork – Tabulky v HTML [cit. 13. 2. 2023]
  https://www.itnetwork.cz/html-css/webove-stranky/jak-psat-moderni-web-html-tutorial-tabulky-a-seznamy

---

# Zdroje

- ITnetwork – Základní CSS selektory a vlastnosti [cit. 10. 4. 2023]
  https://www.itnetwork.cz/html-css/webove-stranky/jak-psat-moderni-web-html-tutorial-zakladni-css-selektory-atributy

- MDN – Introduction to the CSS basic box model [cit. 1. 5. 2023]
  https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Box_Model/Introduction_to_the_CSS_box_model
